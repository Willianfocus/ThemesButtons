import React, { useState } from 'react';


import { ThemeProvider } from 'styled-components'
import THEMES from '../../constants/theme'
import { getTheme } from '../../getTheme'
import * as S from "./styles";

import Header from '../header'

function Main() {
    const [themeName, setThemeName] = useState(THEMES.BASIC);
    return (
        <ThemeProvider theme={getTheme(themeName)}>
      

        <Header />

            <button onClick={() => setThemeName(THEMES.LIGHT)}>Light</button>
            <button onClick={() => setThemeName(THEMES.DARK)}>Dark</button>
            <button onClick={() => setThemeName(THEMES.LIGHTRED)}>Red</button>
            <button onClick={() => setThemeName(THEMES.BLUE)}>Blue</button>
        
        </ThemeProvider>
    )
}

export default Main