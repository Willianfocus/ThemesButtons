import styled from 'styled-components'

export const Container = styled.div`
    background: ${props => props.theme.background};
    height: 5rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    color: ${props => props.theme.color};
    border-bottom: 5px solid ${props => props.theme.secundary};
`
