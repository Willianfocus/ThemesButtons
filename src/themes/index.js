import BASIC from './basic'
import LIGHT from './light'
import DARK from './dark'
import LIGHTRED from './lightred'
import BLUE from './blue'

export {
    BASIC, LIGHT, DARK, LIGHTRED, BLUE,
}