import BASIC from './basic'

export default {
    ...BASIC,
    background: '#0099ff',
    color: '#000',
    secundary: '#00f4',
}