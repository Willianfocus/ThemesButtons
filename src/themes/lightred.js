import BASIC from './basic'

export default {
    ...BASIC,
    background: '#ff1a1a',
    color: '#fff',
    secundary: '#cc0000',
}