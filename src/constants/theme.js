const THEMES = {
    DARK: 'dark',
    LIGHT: 'light',
    LIGHTRED: 'lightred',
    BLUE: 'blue',
    BASIC: 'basic',
};

export default THEMES;