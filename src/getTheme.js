import { DARK, LIGHT, BASIC, LIGHTRED, BLUE } from './themes/'
import THEMES from './constants/theme'

export const getTheme = themeName => {
    switch (themeName) {
        case THEMES.DARK: return DARK;
        case THEMES.LIGHT: return LIGHT;
        case THEMES.LIGHTRED: return LIGHTRED;
        case THEMES.BLUE: return BLUE;
        default: return BASIC
    }
}