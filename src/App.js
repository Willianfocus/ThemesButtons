import React, { useState } from 'react';
import * as S from "./styles";

import { ThemeProvider } from 'styled-components'
import THEMES from './constants/theme'
import { getTheme } from './getTheme'

import Header from './components/header';
import Footer from './components/footer'


function App() {
  const [themeName, setThemeName] = useState(THEMES.BASIC);

  return (
    <ThemeProvider theme={getTheme(themeName)}>
      
        <Header />

        <Footer />

        <S.Button>
          <button onClick={() => setThemeName(THEMES.LIGHT)}>Light</button>
          <button onClick={() => setThemeName(THEMES.DARK)}>Dark</button>
          <button onClick={() => setThemeName(THEMES.LIGHTRED)}>Red</button>
          <button onClick={() => setThemeName(THEMES.BLUE)}>Blue</button>
        </S.Button>
    
      
    </ThemeProvider>
  );
}

export default App;
